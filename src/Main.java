

public class Main {

    public static void main(String[] args) {
        CommandLineUserInterface commandLineUserInterface = new CommandLineUserInterface();
        NumberGenerator numberGenerator = new NumberGenerator();
        System.out.print("Enter your name: ");
        String name = commandLineUserInterface.getUserInput();
        System.out.println("Let the game begin! (if you want close the game, please enter 'exit'");
        int hiddenNumber = numberGenerator.generateNumber();
        while (true){

            String temp = commandLineUserInterface.getUserInput();
            if(temp.equals("exit")){
                System.out.println("Good bye");
                break;
            }
            int number = Integer.parseInt(temp);
            if (number == hiddenNumber){
                System.out.println("Congratulations " + name + "!");
                break;
            } else if(number > hiddenNumber){
                System.out.println("Your number is too big. Please, try again.");

            } else {
                System.out.println("Your number is too small. Please, try again.");
            }

        }

    }
}