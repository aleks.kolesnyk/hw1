import java.util.Scanner;

public class CommandLineUserInterface {
    public String getUserInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
